require 'spec_helper'

describe EventsController do
  let(:tomorrow) { Time.current + 1.day }
  let(:yesterday) { Time.current - 1.day }
  let(:somewhere) { Place.create!(name: 'Somewhere') }

  let(:user) { double('User') }
  let(:event_category) { EventCategory.create!}

  before do
    user.stub(:events).and_return(Event.all)
    User.stub(:first).and_return(user)
  end

  it 'returns 200 OK' do
    get :index
    expect(response).to be_ok
  end

  it 'sorts events by name' do
    event_B = Event.create!(name: 'Bbbb', event_category: event_category, starts_at: tomorrow)
    event_A = Event.create!(name: 'Aaaa', event_category: event_category, starts_at: tomorrow)

    get :index, filters: {}, sort: 'events.name'

    expect(response).to be_ok
    expect(assigns(:events).first).to eql(event_A)
  end

  it 'sorts events by start_at' do
    event_B = Event.create!(name: 'Bbbb', event_category: event_category, starts_at: tomorrow)
    event_A = Event.create!(name: 'Aaaa', event_category: event_category, starts_at: yesterday)

    get :index, filters: {type: :all}, sort: 'starts_at'

    expect(response).to be_ok
    expect(assigns(:events).first).to eql(event_A)
  end

  context "applies type filter" do
    before do
      Event.create!(name: 'Tomorrow', event_category: event_category, starts_at: tomorrow)
      Event.create!(name: 'Yesterday', event_category: event_category, starts_at: yesterday)
    end

    it 'applies type ALL filter' do
      get :index, filters: {type: :all}
      expect(assigns(:events).count('events.id')).to eql(2)
    end

    it 'applies type VALID filter' do
      get :index, filters: {type: :valid}
      expect(assigns(:events).count('events.id')).to eql(1)
      expect(assigns(:events).first.name).to eql('Tomorrow')
    end

    it 'applies type EXPIRED filter' do
      get :index, filters: {type: :expired}
      expect(assigns(:events).count('events.id')).to eql(1)
      expect(assigns(:events).first.name).to eql('Yesterday')
    end
  end

  context "applies place filter" do
    before do
      Event.create!(name: 'Tomorrow', event_category: event_category, starts_at: tomorrow, places: [somewhere])
    end

    it 'includes places' do
      get :index, filters: {place_id: somewhere.id}
      expect(assigns(:events).count('events.id')).to eql(1)
    end

    it 'excludes places' do
      get :index, filters: {place_id: 'somewhere else'}
      expect(assigns(:events).count('events.id')).to eql(0)
    end

  end

  context "applies category filter" do
    before do
      Event.create!(name: 'Tomorrow', event_category: event_category, starts_at: tomorrow)
    end

    it 'includes category' do
      get :index, filters: {event_category_id: event_category.id}
      expect(assigns(:events).count('events.id')).to eql(1)
    end

    it 'excludes places' do
      get :index, filters: {event_category_id: 'another category'}
      expect(assigns(:events).count('events.id')).to eql(0)
    end

  end

  it 'honors place_id parameter' do
    # ToDo, this is a feature to be removed because it's a duplication of filter by place_id
    Event.create!(name: 'Tomorrow', event_category: event_category, starts_at: tomorrow, places: [somewhere])
    get :index, place_id: somewhere.id, filters: {}
    expect(assigns(:events).count('events.id')).to eql(1)
  end

  context 'applies full text search' do
    before do
      Event.create!(name: 'Tomorrow including strange text', event_category: event_category, starts_at: tomorrow)
    end

    it do
      get :index, filters: {}, query: 'strange'
      expect(assigns(:events).count('events.id')).to eql(1)
    end

    it do
      get :index, filters: {}, query: 'stroange'
      expect(assigns(:events).count('events.id')).to eql(0)
    end
  end
end
