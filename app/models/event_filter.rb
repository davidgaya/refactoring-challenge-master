class EventFilter

  attr_reader :params

  def initialize(initial_query)
    @query = initial_query
  end

  def filter(filter_parameters)
    @params = filter_parameters
    if params
      filters_by_type
      filters_by_place_id
      filters_by_event_category_id
    else
      @query = @query.none
    end
    @query
  end

  def filters_by_type
    return @query = @query.valid if params[:type].blank?

    @query =
        case params[:type].try(:to_sym)
          when :all
            @query
          when :valid, :expired
            @query.send(params[:type])
          else
            @query.valid
        end
  end

  def filters_by_place_id
    return @query if params[:place_id].blank?

    @query = @query.by_place_id(params[:place_id].to_i)
  end

  def filters_by_event_category_id
    return @query if params[:event_category_id].blank?

    @query = @query.where(:event_category_id => params[:event_category_id].to_i)
  end

end
