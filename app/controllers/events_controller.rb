class EventsController < ApplicationController

  def index
    sanitize_place_id_filter # ToDo, This is a bad use of parameters that should be cleaned wherever it is used

    @events = EventFilter.new(base_events)
      .filter(params[:filters])
      .order(params[:sort]).order("events.name ASC")
      .full_text_search(params[:query])
  end

  def base_events
    current_user.events.select('
        events.id,
        events.user_id,
        events.name,
        created_by,
        starts_at,
        ends_at,
        event_category_id,
        active
      ').joins(:event_category).preload(:event_category)
  end

  def sanitize_place_id_filter
    params[:filters][:place_id] = params[:place_id] if params[:place_id].present?
  end

end
